#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include "../../SharedMemoryService/shared_memory.h"
#include "../../WebSocketCPP/server_ws.hpp"
#include "../../WebSocketCPP/crypto.hpp"
#include "../WebSocketAPI/server/websocket_api.h"
#include <pthread.h>

using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;

class Communicator : public WebsocketApi
{
private:
public:
  bool DATAHANDLER_FLAG;
  WsServer cockpitWSServer;
  pthread_t cockpitDataServer_Thread;
  pthread_t wsSocketServer_Thread;
  pthread_t databaseUpdate_Thread;
  pthread_t messageSet_Thread;
  Communicator();
  void InitializeSharedMemory();
  void InitializeThreads();

  static void *CockpitDataServer(void *arguments);
  static void *DatabaseUpdate(void *arguments);
  static void *StartWsServer(void *arguments);
  static void *MessageSet(void *arguments);

  std::string to_string_with_precision(double value, int n);
  bool format_checker(std::string message_str);

  void exitCommunicator();
};

#endif
