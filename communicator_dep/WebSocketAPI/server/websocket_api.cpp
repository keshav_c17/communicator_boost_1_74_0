#include "websocket_api.h"

void WebsocketApi::StartServer()
{
  if (!(this->server_endpoints_init_status))
  {
    if (this->require_broadcast)
    {
      this->BroadcastEndpoint();
    }
    if (this->require_private)
    {
      this->PrivateEndpoint();
    }
    this->server_endpoints_init_status = true;
  }
  pthread_create(&server_thread, NULL, WebsocketApi::server_handler, this);
  usleep(10000);
  // pthread_create(&broadcast_thread, NULL, WebsocketApi::broadcast_data, this);
}

void *WebsocketApi::server_handler(void *c)
{
  std::cout << "[WebSocketApi] Initiated Server" << std::endl;
  ((WebsocketApi *)c)->server.start();
}

void WebsocketApi::StopServer()
{
  this->ExitBroadcast();
  this->server.stop();
}

void WebsocketApi::BroadcastEndpoint()
{
  auto &bc = server.endpoint["^/broadcast/?$"];
  bc.on_message = [&](std::shared_ptr<WsServer::Connection> connection,
                      std::shared_ptr<WsServer::InMessage> message) {
    std::string msg = message->string();
    if (this->terminal_ouputs)
    {
      std::cout << "[Broadcast] Message Recieved: " << msg
                << " from: " << (size_t)connection.get()
                << " Connections Count: " << this->bc_conn_list.size()
                << std::endl;
    }

  };

  bc.on_open = [&](std::shared_ptr<WsServer::Connection> connection) {
    this->BroadcastOnopen(connection);
    this->add_connection(connection, this->bc_conn_list);
    if (this->terminal_ouputs)
    {
      std::cout << "[Broadcast] Server: Opened connection "
                << (size_t)connection.get()
                << " Connections Count: " << this->bc_conn_list.size()
                << std::endl;
    }
    // std::cout<<"Testing On open"<<std::endl;
  };

  bc.on_close = [&](std::shared_ptr<WsServer::Connection> connection, int status,
                    const std::string & /*reason*/) {
    this->BroadcastOnclose(connection);
    this->rm_connection(connection, this->bc_conn_list);
    if (this->terminal_ouputs)
    {
      std::cout << "[Broadcast] Server: Closed connection "
                << (size_t)connection.get() << " with status code " << status
                << " Connections Count: " << this->bc_conn_list.size()
                << std::endl;
    }
  };

  bc.on_error = [&](std::shared_ptr<WsServer::Connection> connection,
                    const boost::system::error_code &ec) {
    this->BroadcastOnerror(connection);
    this->rm_connection(connection, this->bc_conn_list);
    if (this->terminal_ouputs)
    {
      std::cout << "[Broadcast] Server: Error in connection "
                << (size_t)connection.get() << ". "
                << "Error: " << ec << ", error message: " << ec.message()
                << std::endl;
    }
  };
  std::cout << "[WebSocketApi] Broadcast Endpoint Setup Completed" << std::endl;
}

void *WebsocketApi::broadcast_data(void *c)
{
  std::cout << "[WebSocketApi] Started broadcasting messages" << std::endl;
  while (!((WebsocketApi *)c)->exit_broadcast)
  {
    if (((WebsocketApi *)c)->broadcast_data_status)
    {
      ((WebsocketApi *)c)->SendBroadcastMessage();
    }
    usleep(((WebsocketApi *)c)->broadcast_delay_in_micro_sec);
  }
  ((WebsocketApi *)c)->exit_broadcast_status = true;
}

void WebsocketApi::SendBroadcastMessage()
{
  auto message_str = this->broadcast_msg;
  auto send_stream = std::make_shared<WsServer::OutMessage>();
  // std::cout << "Server Side : " << message_str << std::endl;
  *send_stream << message_str;
  for (int i = 0; i < bc_conn_list.size(); i++)
  {
    bc_conn_list.at(i).connection->send(send_stream,
                                        [](const boost::system::error_code &ec) {
                                          if (ec)
                                          {
                                            std::cout
                                                << "[Server Error][Broadcast] sending message. "
                                                << "[Error] " << ec << ", [Error Message] "
                                                << ec.message() << std::endl;
                                          }
                                        });
  }
}

void WebsocketApi::StartBroadcast()
{
  broadcast_data_status = true;
}
void WebsocketApi::StopBroadcast()
{
  broadcast_data_status = false;
}
void WebsocketApi::ExitBroadcast()
{
  this->exit_broadcast = true;
  while (!(this->exit_broadcast_status))
  {
    usleep(100000);
  }
  usleep(100000);
}
void WebsocketApi::SetBroadcastMessage(std::string msg)
{
  broadcast_msg = msg;
}
void WebsocketApi::BroadcastOnmessage(
    std::shared_ptr<WsServer::Connection> connection, std::string msg)
{
  // overide the function to add a functionality
}
void WebsocketApi::BroadcastOnopen(
    std::shared_ptr<WsServer::Connection> connection)
{
  // overide the function to add a functionality
}
void WebsocketApi::BroadcastOnclose(
    std::shared_ptr<WsServer::Connection> connection)
{
  // overide the function to add a functionality
}
void WebsocketApi::BroadcastOnerror(
    std::shared_ptr<WsServer::Connection> connection)
{
  // overide the function to add a functionality
}

void WebsocketApi::PrivateEndpoint()
{
  auto &p = this->server.endpoint["^/broadband/?$"];

  p.on_message = [&](std::shared_ptr<WsServer::Connection> connection,
                     std::shared_ptr<WsServer::InMessage> message) {
    std::string msg = message->string();
    this->PrivateOnmessage(connection, msg);

    if (this->terminal_ouputs)
    {
      std::cout << "[Private] Message Recieved: " << msg
                << " from: " << (size_t)connection.get()
                << " Connections Count: " << this->p_conn_list.size()
                << std::endl;
    }
  };

  p.on_open = [&](std::shared_ptr<WsServer::Connection> connection) {
    this->PrivateOnopen(connection);
    this->add_connection(connection, this->p_conn_list);
    if (this->terminal_ouputs)
    {
      std::cout << "[Private] Server: Opened connection "
                << (size_t)connection.get()
                << " Connections Count: " << this->p_conn_list.size()
                << std::endl;
    }
  };

  p.on_close = [&](std::shared_ptr<WsServer::Connection> connection, int status,
                   const std::string & /*reason*/) {
    this->PrivateOnclose(connection);
    this->rm_connection(connection, this->p_conn_list);
    if (this->terminal_ouputs)
    {
      std::cout << "[Private] Server: Closed connection "
                << (size_t)connection.get() << " with status code " << status
                << " Connections Count: " << this->p_conn_list.size()
                << std::endl;
    }
  };

  p.on_error = [&](std::shared_ptr<WsServer::Connection> connection,
                   const boost::system::error_code &ec) {
    this->PrivateOnerror(connection);
    this->rm_connection(connection, this->p_conn_list);
    if (this->terminal_ouputs)
    {
      std::cout << "[Private] Server: Error in connection "
                << (size_t)connection.get() << ". "
                << "Error: " << ec << ", error message: " << ec.message()
                << std::endl;
    }
  };
  std::cout << "[WebSocketApi] Private Endpoint Setup Completed" << std::endl;
}

void WebsocketApi::PrivateOnmessage(
    std::shared_ptr<WsServer::Connection> connection, std::string msg)
{
  // overide the function to add a functionality
}
void WebsocketApi::PrivateOnopen(
    std::shared_ptr<WsServer::Connection> connection)
{
  // overide the function to add a functionality
}
void WebsocketApi::PrivateOnclose(
    std::shared_ptr<WsServer::Connection> connection)
{
  // overide the function to add a functionality
}
void WebsocketApi::PrivateOnerror(
    std::shared_ptr<WsServer::Connection> connection)
{
  // overide the function to add a functionality
}

void WebsocketApi::PrivateSend(std::shared_ptr<WsServer::Connection> connection,
                               std::string msg)
{
  auto message_str = msg;

  auto send_stream = std::make_shared<WsServer::OutMessage>();
  *send_stream << message_str;

  connection->send(send_stream, [](const boost::system::error_code &ec) {
    if (ec)
    {
      std::cout << "[Server Error][Private] sending message. "
                << "[Error] " << ec << ", [Error Message] " << ec.message()
                << std::endl;
    }
  });
}

void WebsocketApi::rm_connection(
    std::shared_ptr<WsServer::Connection> connection,
    std::deque<_connection> &list)
{
  _connection conn;
  conn.connection = connection;
  conn.status = false;
  int conn_location = 0;
  for (int i = 0; i < list.size(); i++)
  {
    _connection tmp;
    tmp = list.at(i);
    conn_location = i;
    if (tmp.connection == conn.connection)
    {
      break;
    }
  }
  if (list.size() > 0)
  {
    list.erase(list.begin() + conn_location);
  }
}

void WebsocketApi::add_connection(
    std::shared_ptr<WsServer::Connection> connection,
    std::deque<_connection> &list)
{
  _connection conn;
  conn.connection = connection;
  conn.status = true;
  list.push_back(conn);
}
