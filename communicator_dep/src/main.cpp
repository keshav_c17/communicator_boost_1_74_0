#include "../include/Communicator.h"

#include <iostream>

Communicator Datahandler;

void shutDown()
{
    if (Datahandler.DATAHANDLER_FLAG)
    {
        Datahandler.DATAHANDLER_FLAG = false;
        Datahandler.exit_broadcast_status = true;
        Datahandler.exitCommunicator();

        (void)pthread_join(Datahandler.cockpitDataServer_Thread, NULL);
        std::cout << "Exiting Cockpit Server Thread" << std::endl;

        pthread_join(Datahandler.messageSet_Thread, NULL);
        std::cout << "Exiting Message Set Thread" << std::endl;

        pthread_join(Datahandler.wsSocketServer_Thread, NULL);
        std::cout << "Exiting Web Socket Server Thread" << std::endl;

        pthread_join(Datahandler.databaseUpdate_Thread, NULL);
        std::cout << "Exiting Database Update Server Thread" << std::endl;
    }
}
void handler(int s)
{

    printf("Interupt signal Received --->%d<---- Communicator\n", s);

    // Broken pipe from a client
    if (s == SIGPIPE)
    {

        // Just ignore, sock class will close the client the next time
    }
    // Catch ctrl-c, segmentation fault, abort signal
    if (s == SIGINT || s == SIGSEGV || s == SIGABRT || s == SIGTERM)
    {

        shutDown();
    }
}
void initSigHandler()
{

    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    sigaction(SIGPIPE, &sigIntHandler, NULL);
    sigaction(SIGABRT, &sigIntHandler, NULL);
    sigaction(SIGSEGV, &sigIntHandler, NULL);
    sigaction(SIGTERM, &sigIntHandler, NULL);
}

int main(int argc, char const *argv[])
{
    initSigHandler();
    // std::cout <<  "intialize shared memory " << std::endl;
    // Datahandler.InitializeSharedMemory();
    // std::cout << "start thread " << std::endl;
    Datahandler.InitializeThreads();
    Datahandler.broadcast_data_status = false;
    // std::cout << "Set Message: " << Datahandler.broadcast_msg << std::endl;
    while (Datahandler.DATAHANDLER_FLAG)
    {
        sleep(1);
    }
    return 0;
}
