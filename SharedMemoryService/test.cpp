#include "shared_memory.h"

using namespace boost::interprocess;

int main()
{
  SMemory sms;

  sms.update("Boost_obj", 5, 12.3, 12);
  sms.update_bool("aah", true);
  sms.update_str("lodu", "mai hu");
  sms.update("create ho ja", 400, 9.7, 3);

  std::cout<<sms.fetch("Boost_obj", 7)<<std::endl;
  std::cout<<sms.fetch_str("lodu")<<std::endl;
  std::cout<<sms.fetch_bool("aah")<<std::endl;

  std::cout<<sms.fetch_str("crash")<<std::endl;
  std::cout<<sms.fetch("maihuraja", 1)<<std::endl;
  std::cout<<sms.fetch_bool("Boolo")<<std::endl;
  std::cout<<sms.fetch("Boost_obj dsd", 3)<<std::endl;
  return 0;
}