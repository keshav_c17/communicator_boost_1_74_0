#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H
#define VALUE 0

//#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <iostream>
#include <string>

class SMemory {
  private:

  bool checkMemory(std::string);
  void create(std::string name, unsigned int data_size);
  void create_bool(std::string name);
  void create_str(std::string name);

public:

  double fetch(std::string name, unsigned int arr_location);
  bool fetch_bool(std::string name);
  std::string fetch_str(std::string name);

  void update(std::string name, unsigned int arr_location, double value, unsigned int array_size);
  void update_bool(std::string name, bool value);
  void update_str(std::string name, std::string value);
};

#endif
